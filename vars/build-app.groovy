#!/usr/bin/env groovy

import org.example.Constants

def call (Map config=[:])
{
    
    pipeline
    {

         agent  {label 'docker-agent'}
 
        parameters{
                 booleanParam(name: 'skipBuild',
                              defaultValue: false,
                              description: 'well, no description'
                              )
                choice(name: 'BuildOnly',
                       
                       choices: 'no\nyes'
                       )
  }

stages {
  stage('Build') {
    when {
      anyOf {
        expression { params.skipBuild == 'true'}
        expression { params.BuildOnly == 'yes'}
      }
    }
    steps{
           sh """ echo 'hellow world'
            pwd
            ls -ltr
           echo $DOCKER_HOST
           echo "passed values ${config.AppName}"
           """
    }
  }
  stage('Test'){
    environment { githubuser = credentials('gitlab') 
                 AWS_ACCESS_KEY_ID = "${githubuser_USR}"
                 docker_version="18.03.1-ce"
    }
    when {
      anyOf {
        expression {1 == 2}
      }
    }
    steps{
             sh """
                    echo "hellow world"

             """
              
            
             
    }
  }
}
    }
}